import React from 'react'
import styled from 'styled-components'
import { Badge } from '@material-ui/core'
import { ShoppingCartOutlined } from '@material-ui/icons'
import { mobile } from '../responsive'
import { useSelector } from 'react-redux'

const Navbar = () => {
const user = useSelector(state=> state.user.currentUser);
  return (
    <Container>
        <Wrapper>
            <Left>
                <Language>EN</Language>
            </Left>
            <Center>
                <Logo>
                    SAMPLE.
                </Logo>
            </Center>
            <Right>
                {!user && 
                    <>
                     <MenuItem>
                    <Link href='/register'>
                        REGISTER
                    </Link>
                </MenuItem>
                <MenuItem>
                    <Link href='/login'>
                        LOGIN
                    </Link>
                </MenuItem>
                   
                    </>
                }
                <MenuItem>
                    <Badge badgeContent={4} color='primary'>
                        <ShoppingCartOutlined />                    
                    </Badge> 
                </MenuItem>
            </Right>
        </Wrapper>
    </Container>
  )
}

export default Navbar

const Container = styled.div`
    height: 60px;
    ${mobile({ height : "50px"})};
`
const Wrapper = styled.div`
    padding: 10px 20px;
    display:flex;
    justify-content:space-between;
    ${mobile({ padding : "10px 0px"})};
`

const Language = styled.span`
    font-size: 14px;
    cursor: pointer;
`

const Left = styled.div`
    flex: 1;
`
const Center = styled.div`
    flex: 1;
    text-align: center;
`
const Right = styled.div`
    flex: 1;
    display: flex;
    align-items: center;
    justify-content: flex-end;
`

const Logo = styled.h1`
    font-weight: bold;
`

const MenuItem = styled.div`
    font-size: 14px;
    cursor:pointer;
    margin-left: 25px;
`

const Link = styled.a`
    text-decoration: none;
    font-size: 14px;
    color: black;
`