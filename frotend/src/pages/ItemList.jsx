import styled from "styled-components";
import Navbar from "../components/Navbar";
import Items from "../components/Items";
import Footer from "../components/Footer";


const ItemList = () => {
  return (
    <Container>
      <Navbar />
      <Title>Items</Title>
      <Items />
      <Footer />
    </Container>
  );
};

export default ItemList;

const Container = styled.div``;

const Title = styled.h1`
  margin: 20px;
`;
