const express = require('express');
const app = express();
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const userRoute = require('./routes/user')
const authRoute = require('./routes/auth')
const itemRoute = require('./routes/item');
const cors = require('cors');

dotenv.config();

mongoose.connect(process.env.MONGO_URL)
    .then(() => console.log('DB success'))
    .catch((err) => console.log(err))

app.use(cors());
app.use(express.json())
app.use('/users', userRoute)
app.use('/auth', authRoute)
app.use('/items', itemRoute)

app.listen(process.env.PORT || 5000, () => {
    console.log('Backend server is running!')
});