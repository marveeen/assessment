import Home from './pages/Home'
import Login from './pages/Login'
import Register from './pages/Register'
import Item from './pages/Item'
import ItemList from './pages/ItemList'
import { Route, Routes, Navigate, BrowserRouter } from 'react-router-dom'
import { useSelector } from 'react-redux'

const App = () => {
  const user = useSelector(state=> state.user.currentUser);
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/items' element={<ItemList />} />
          <Route path='/item/:id' element={<Item />} />
          <Route 
            path='/login' 
            element={ user ? <Navigate to='/' /> :<Login />} 
          />
          <Route 
            path='/register' 
            element={ user ? <Navigate to='/' /> :<Register />} 
          />
        </Routes>
      </BrowserRouter>
    </div>
  )
}

export default App