import React from 'react'
import Footer from '../components/Footer'
import Navbar from '../components/Navbar'
import ItemList from '../components/Items'

const Home = () => {
  return (
    <div>
        <Navbar />
        <ItemList />
        <Footer />
    </div>
  )
}

export default Home