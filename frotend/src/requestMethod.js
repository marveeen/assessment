import axios from 'axios'

const BASE_URL = "http://localhost:5000/"
const TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyNTUwZjA1Y2MwNDI5MzdkZTM4OWUxYiIsImlhdCI6MTY0OTc0ODQ4MiwiZXhwIjoxNjUwMDA3NjgyfQ.uCIR8bXnQThmnaE6NbCxdJwSeZYEInTBCm43Pazikks";

export const publicRequest = axios.create({
    baseURL: BASE_URL,
})

export const userRequest = axios.create({
    baseURL: BASE_URL,
    header: { token: `Bearer ${TOKEN}`}
})