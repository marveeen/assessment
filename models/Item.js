const mongoose = require('mongoose');

const ItemSchema = new mongoose.Schema({
    product_name: {
        type: String,
        required: true,
    },
    quantity: {
        type: Number,
        required: true,
        min: [1, 'Quantity can not be less then 1.'],
        deafult: 1
    },
    img: {
        type: String,
        required: true
    },
    isActive:{
        type: String,
    },
    date_added: {
        type: Date,
        default: Date.now
    },
});

module.exports = mongoose.model('Item',ItemSchema);