const router = require('express').Router();
const User = require('../models/User');
const CryptoJS = require('crypto-js')
const jwt = require('jsonwebtoken')

//Register
router.post('/register', async (req,res) => {
    const newUser = new User({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        password: CryptoJS.AES.encrypt(
            req.body.password, 
            process.env.PASS_SEC
        ).toString(),
    });

    try {
        const savedUser = await newUser.save();
        res.status(200).json(savedUser)
    } catch (err) {
        res.status(500).json(err);
    }
    
})
    
//LOGIN
router.post('/login', async (req,res) => {
    try {
        const email = await User.findOne({ email: req.body.email });
        !email && res.status(401).json('Wrong Credentials')

        const hashedPassword = CryptoJS.AES.decrypt(
            email.password,
            process.env.PASS_SEC
        );

        const orginalPassword = hashedPassword.toString(CryptoJS.enc.Utf8);

        orginalPassword !== req.body.password &&
            res.status(401).json('Wrong credentials');
        
        const accessToken = jwt.sign(
            {
                id:email.id
            },
            process.env.JWT_SEC,
            {
                expiresIn: "3d"
            }
        )

        const { password, ...others } = email._doc;

        res.status(200).json({...others, accessToken})
    } catch (err) {
        res.status(500).json(err);
        return
    }
})


module.exports = router;