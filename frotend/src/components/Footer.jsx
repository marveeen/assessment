import { Facebook, Instagram, MailOutline, Phone, Twitter, Room } from '@material-ui/icons'
import styled from 'styled-components'

const Footer = () => {
  return (
    <Container>
        <Left>
            <Logo>SAMPLE</Logo>
            <Desc>Simple login and Product website</Desc>
            <SocialContainer>
                <SocialIcon color="3B5999">
                    <Facebook />
                </SocialIcon>
                <SocialIcon color="E4405F">
                    <Instagram />
                </SocialIcon>
                <SocialIcon color="55ACEE">
                    <Twitter />
                </SocialIcon>
            </SocialContainer>
        </Left>
        <Center>
            <Title>Useful Links</Title>
            <List>   
                <ListItem>Home</ListItem>
                <ListItem>My Account</ListItem>
            </List>
        </Center>
        <Right>
            <Title>Contact</Title>
            <ContactItem>
                <Room style={{marginRight: "10px"}}/> Marilao, Bulacan
            </ContactItem>
            <ContactItem>
                <Phone style={{marginRight: "10px"}}/> 09756478215
            </ContactItem>
            <ContactItem>
               <MailOutline style={{marginRight: "10px"}}/> sample@contact.com
            </ContactItem>
        </Right>
    </Container>
  )
}

export default Footer

const Container = styled.div`
    display: flex;

`
const Left = styled.div`
    flex: 1;
    display:flex;
    flex-direction: column;
    padding: 20px;
`
const Center = styled.div`
    flex: 1;
    padding: 20px;
`
const Title = styled.h3`
    margin-bottom: 20px;
`
const List = styled.ul`
    text-wrap: wrap;
`
const ListItem = styled.li`
    width: 50%;
    margin-bottom: 10px;    
`
const Right = styled.div`
    flex: 1;
    padding: 20px;
`
const Logo = styled.h1``
const Desc = styled.p`
    margin: 20px 0px;
`
const SocialContainer = styled.div`
    display: flex;
`
const SocialIcon = styled.div`
    width: 40px;
    height: 40px;
    border-radius: 50%;
    color: white;
    background-color: #${props=> props.color};
    display: flex;
    align-items: center;
    justify-content: center;
`
const ContactItem = styled.div`
    margin-bottom: 20px;
    display: flex;
    align-items: center;-
`

