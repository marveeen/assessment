const Item = require('../models/Item');
const { verifyToken, verifyTokenAndAuthorization} = require('./verifyToken');
const router = require('express').Router();
// CREATE

router.post('/', verifyToken, async(req,res) => {
    const newItem = new Item(req.body);

    try {
        const saveItem = await newItem.save();
        res.status(200).json(saveItem);
    } catch (err) {
        res.status(500).json(err)
    }
})

router.put('/:id', verifyToken, async(req,res) => {

    try {
        const updatedItem = await Item.findByIdAndUpdate(
            req.params.id,
            {
                $set: req.body,
            },
            { new: true }
            
        );
        res.status(200).json(updatedItem);
    } catch (err) {
        res.status(500).json(err)
    }
})

router.delete('/:id', verifyToken, async(req,res) => {
    try {
        await Item.findByIdAndDelete(req.params.id);
        res.status(200).json('Item has been Deleted')
    } catch (err) {
        res.status(500).json(err);
    }
})

//get Product
router.get('/:id', async(req,res) => {
    try {
        const item = await Item.findById(req.params.id);
        res.status(200).json(item);
    } catch (err) {
        res.status(500).json(err)
    }
})

router.get('/', async(req,res) => {
    try {
        const item = await Item.find();
        res.status(200).json(item);
    } catch (err) {
        res.status(500).json(err)
    }
})


//Get all product

module.exports = router;