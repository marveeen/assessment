import axios from "axios";
import { useState, useEffect } from "react";
import styled from "styled-components";
import { fakeItem } from "../fakeData";
import Item from "./Item";

const Items = () => {
  const [products, setProducts] = useState([])

  useEffect(() => {
    const getProducts = async () => {
      try {
        const res = await axios.get('http://localhost:5000/items')
        console.log(res);
      } catch (err) {
        console.log(err)
      }
    }
    getProducts()
  })
  
  return (
    <Container>
      {fakeItem.map((item) => (
        <Item item={item} key={item.id} />
      ))}
    </Container>
  );
};

export default Items;

const Container = styled.div`
    padding: 20px;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
`;