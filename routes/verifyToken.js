const jwt = require('jsonwebtoken')

const verifyToken = (req, res, next) => {
    const authHeader = req.headers.token
    if(authHeader) {
        const token = authHeader.split(" ")[1];
        jwt.verify(token, process.env.JWT_SEC, (err, data) => {
            if(err) res.status(403).json('Token is not valid!');
            req.email = data;
            next();
        })
    } else {
        return res.status(401).json('You are not authorize')
    }
}

const verifyTokenAndAuthorization = (req, res, next) => {
    verifyToken(req,res, () => {
        if (req.email.id === req.params.id){
            next()
        } else {
            return res.status(403).json('You are not allowed to access')
        }
    })
}

module.exports = { verifyToken, verifyTokenAndAuthorization };