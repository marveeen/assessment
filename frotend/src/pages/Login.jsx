import styled from 'styled-components'
import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { login } from '../redux/apiCalls'

const Login = () => {
    const [user, setUser] = useState({
        email: "",
        password: ""
    })
    const dispatch = useDispatch()
    const { isFetching, error} = useSelector((state) => state.user)

    const handleChange = (e) => {
        let { name, value } = e.target
        setUser(prev => ({
            ...prev,
            [name]: value
        }))
    }

    const { email, password} = user

    const handleClick = (e) => {
        e.preventDefault()
        login(dispatch, { email, password})
    }

    return (
        <Container>
            <Wrapper>
                <Title>SIGN IN</Title>
                <Form>
                    <Input 
                        placeholder="email"
                        onChange ={(e) => handleChange(e)}
                        name="email"
                        value={user.email}
                    />
                    <Input 
                        placeholder="password"
                        type="password"
                        onChange={(e) => handleChange(e)}
                        name="password"
                        value={user.password}
                    />
                    <Button 
                        onClick={(e) => handleClick(e)}
                        disabled={isFetching}
                    >LOGIN</Button>
                    {error && <Error>Something went wrong..</Error>}
                    <Link>CREATE A NEW ACCOUNT</Link>
                </Form>
            </Wrapper>
        </Container>
    )
}

export default Login

const Container = styled.div`
    width: 100vw;
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
`
const Wrapper = styled.div`
    width: 40%;
    padding: 20px;
    background-color: white;
    
`
const Form = styled.div`
    display: flex;
    flex-direction: column;
`
const Title = styled.h1`
    font-size: 24px;
    font-weight: 300;
`
const Input = styled.input`
    flex: 1;
    min-width: 40%;
    margin: 10px 0;
    padding: 10px;
`
const Button = styled.button`
    margin: 20px 0;
    width: 40%;
    boder: none;
    padding: 15px 20px;
    background-color: teal;
    color: white;
    cursor: pointer;
    margin-bottom: 10px
    &::disabled{
        color: green;
        cursor: not-allowed;
    }
`
const Link = styled.a`
    margin: 5px 0px;
    font-size: 12px;
    text-decoration: underline;
    cursor: pointer;

`
const Error = styled.span`
    color:red;
`