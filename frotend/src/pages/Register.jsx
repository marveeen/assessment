import styled from 'styled-components'

const Register = () => {
  return (
    <Container>
        <Wrapper>
            <Title>
                CREATE AN ACCOUNT
            </Title>
            <Form>
                <Input placeholder="first name" />
                <Input placeholder="last name" />
                <Input placeholder="email" />
                <Input placeholder="username" />
                <Input placeholder="confirm password" />
                <Button>Create</Button>
            </Form>
        </Wrapper>
    </Container>
  )
}

export default Register

const Container = styled.div`
    width: 100vw;
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
`
const Wrapper = styled.div`
    width: 40%;
    padding: 20px;
    background-color: white;
    
`
const Form = styled.div`
    display: grid;
    grid-template-columns: 1fr;
`
const Title = styled.h1`
    font-size: 24px;
    font-weight: 300;
`
const Input = styled.input`
    flex: 1;
    min-width: 40%;
    margin: 20px 10px 0 0;
    padding: 10px;
`
const Button = styled.button`
    margin: 20px 0;
    width: 40%;
    boder: none;
    padding: 15px 20px;
    background-color: teal;
    color: white;
    cursor: pointer;
`