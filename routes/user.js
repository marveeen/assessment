const User = require('../models/User')
const { verifyToken, verifyTokenAndAuthorization } = require('./verifyToken');

const router = require('express').Router();


// Updated
router.put('/:id', verifyTokenAndAuthorization, async (req, res) => {
   if(req.body.password) {
        req.body.password = CryptoJS.AES.encrypt(
            req.body.password, 
            process.env.PASS_SEC
        ).toString();
   }

   try {
       const updatedUser = await User.findByIdAndUpdate(
           req.params.id, {
               $set: req.body
           }, {new:true}
       );

       res.status(200).json(updatedUser)
   } catch (err) {
       res.status(500).json(err);
   }
})

//Delete

router.delete("/:id", verifyTokenAndAuthorization, async (req,res) => {
    try {
        await User.findByIdAndDelete(req.params.id)
        res.status(200).json("User deleted")
    } catch (err){
        res.status(500).json(error)
    }
})

//GET USER
router.get("/:id", verifyTokenAndAuthorization, async (req,res) => {
    try {
        const email = await User.findById(req.params.id)
        const { password, ...others } = email._doc;

        res.status(200).json(others)
    } catch (err){
        res.status(500).json(err)
    }
})

//get all user
router.get('/', verifyToken, async(req, res) => {
    const query = req.query.new;
    try {
        const users = query
            ? await User.find().sort({ _id: -1 }).limits(5)
            : await User.find()
        res.status(200).json(users);
    } catch (err) {
        res.status(500).json(err)
    }
})

module.exports = router